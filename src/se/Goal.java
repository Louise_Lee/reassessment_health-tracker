package se;

import java.util.Date;

/**
 * 5012B Implementation Reassessment
 * Health Tracker
 * @author Louise Lee
 */
public class Goal {
    
    private String Name;
    private String ID;
    private String Type;
    private Date Deadline;
    private boolean Achieved;
    private int target;

    /**
     *
     * Create a new goal with certain details
     * 
     * @param Name      Goal name
     * @param Deadline  Deadline of this goal
     * @param target    Goal target
     */
    public Goal(String Name, Date Deadline, int target){
    
        this.ID = ProfileGUI.u.getID();
        this.Name = Name;
        this.Deadline = Deadline;
        this.target = target;

    }
    
    /**
     * Set Name to another Name
     * 
     * @param Name New name of goal
     */
    public void setName(String Name){
    
        this.Name = Name;
    
    }
 
    /**
     * Set ID to another ID
     * 
     * @param ID New ID of Goal
     */    
    public void setID(String ID){
    
        this.ID = ID;
    
    }    
    
    /**
     * Set Type to another Type
     * 
     * @param Type New Type of Goal
     */        
    public void setType(String Type){

        this.Type = Type;

    }    

    /**
     * Set Deadline to another Deadline
     * 
     * @param Deadline New Deadline of Goal
     */         
    public void setDeadline(Date Deadline){

        this.Deadline = Deadline;
    
    }    
    
    /**
     * Set Achieved to another Achieved
     * 
     * @param Achieved New Achieved of Goal
     */      
    public void setAchieved(boolean Achieved){
    
        this.Achieved = Achieved;
    
    }      
    
    /**
     * Set weight to another weight
     * 
     * @param weight New weight of Goal
     */     
    public void setWeight(int weight) {
        
        this.target = weight;
        
    }

    /**
     * Get Name of this Goal
     * 
     * @return Name of this Goal
     */  
    public String getName(){
    
        return this.Name;
    
    }
 
    /**
     * Get ID of this Goal
     * 
     * @return ID of this Goal
     */      
    public String getID(){
    
        return this.ID;
    
    }    
    
    /**
     * Get Type of this Goal
     * 
     * @return Type of this Goal
     */        
    public String getType(){
    
        return this.Type;
    
    }    

    /**
     * Get Deadline of this Goal
     * 
     * @return Deadline of this Goal
     */     
    public Date getDeadline(){
    
        return this.Deadline;

    }    
 
    /**
     * Get Achieved of this Goal
     * 
     * @return Achieved of this Goal
     */       
    public boolean getAchieved(){
    
        return this.Achieved;
    
    }         
    
    /**
     * Get Target of this Goal
     * 
     * @return Target of this Goal
     */      
    public int getTarget(){
    
        return this.target;
    
    }      
    
    @Override
    public String toString(){
    
        StringBuilder userInfo = new StringBuilder();
        
            userInfo.append("Name: ");
            userInfo.append(getName());
            userInfo.append("\n");
            userInfo.append("ID: ");
            userInfo.append(getID());
            userInfo.append("\n");
            userInfo.append("Type: ");
            userInfo.append(getType());
            userInfo.append("\n");
            userInfo.append("Deadline: ");
            userInfo.append(getDeadline());
            userInfo.append("\n");
            userInfo.append("Achieved: ");
            userInfo.append(getAchieved());
            userInfo.append("\n");
            userInfo.append("Target: ");
            userInfo.append(getTarget());
            userInfo.append("\n");            
        
        return userInfo.toString();

    }    
    
}
