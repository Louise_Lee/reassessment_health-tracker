package se;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * 5012B Implementation Reassessment
 * Health Tracker
 * @author Louise Lee
 */
public class ModelController {

    private FileStorageController files;
    private ArrayList<User> userList;
    private ArrayList<Goal> goalList;
    private ArrayList<HistoryWeight> historyList;
    private ProfileGUI pg;
    
    /**
     * Core of this application
     */
    public ModelController(){
 
        files = new FileStorageController();
        userList = new ArrayList<>();
        ScanUserFile();
        historyList = new ArrayList<>();
        
    }
    
    /**
     * Read the data from user.txt file
     */    
    private void ScanUserFile(){
    
        try {
            
            Scanner scan = new Scanner(files.getUserFile());
            String delim = ",";

            while (scan.hasNextLine()) {
        
                String line = scan.nextLine();
                
                if(!line.isEmpty()){
                    
                    Scanner scan2 = new Scanner(line);
                    scan2.useDelimiter(delim);
                        
                    String ID = scan2.next();
                    String Password = scan2.next();

                    String Name = scan2.next();
                    
                    String gender = scan2.next();
                    String email = scan2.next();
                    
                    String date = scan2.next();
                    Date dob = null;
                    
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {

                        dob = format.parse(date);

                    } catch (java.text.ParseException ex) {

                        ex.printStackTrace();

                    }
                    
                    // Age may change(keep checking when
                    // everytime the programme start)
                    scan2.next();
                    int Age = countAge(dob);
                    
                    int Weight = Integer.parseInt(scan2.next());
                    
                    int height = Integer.parseInt(scan2.next());
                    File photo = new File(scan2.next());                        
                    
                    User u = new User(ID, Password, Name, gender, email,
                                      dob, Age, Weight, height, photo);
                    
                    // Avoid double insert of the same user
                    if(!IDexist(ID)){
                    
                        userList.add(u);
                        
                    }
                    
                }

            }
            
        } catch (FileNotFoundException ex) {
            
            ex.printStackTrace();
            
        }

    }

    /**
     * Read the data from goal.txt file
     */        
    public void ScanGoalFile(){
    
        goalList = new ArrayList<>();
        
        try {
            
            Scanner scan = new Scanner(files.getGoalFile());
            String delim = ",";
            
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            while (scan.hasNextLine()) {
        
                String line = scan.nextLine();
   
                if(!line.isEmpty()){
                
                Scanner scan2 = new Scanner(line);
                scan2.useDelimiter(delim);
                
                    String Name = scan2.next();
                    String ID = scan2.next();
                    String Type = scan2.next();
                    Date Deadline = null;
                    
                    try {

                        Deadline =formatter.parse(scan2.next());

                    } catch (ParseException ex) {

                        ex.printStackTrace();

                    }

                    boolean Achieved = Boolean.parseBoolean(scan2.next());
                
                    int weight = Integer.parseInt(scan2.next());
                    
                    Goal g = new Goal(Name, Deadline, weight);
                    // Avoid current user data overwrite other users data
                    if(!ID.equals(ProfileGUI.u.getID())){
                    
                        g.setID(ID);
                    
                    }
                    g.setType(Type);
                    g.setAchieved(Achieved);
                    
                    if(ProfileGUI.u.getID().equals(ID)){
                        
                        //Check status of the goal(Update will apply basing on the situation)
                        if(new Date().after(Deadline) && g.getType().equals("Current")){
                        
                            g.setAchieved(false);
                            g.setType("Failed");
                        
                        }else if(new Date().before(Deadline)|| Deadline.equals((new Date())) && g.getType().equals("Current")){
                        
                            if(ProfileGUI.u.getWeight()<=weight){
                            
                                g.setAchieved(true);
                                g.setType("Achieved");
                            
                            }else{
                            
                                g.setAchieved(false);
                                g.setType("Current");                            
                            
                            }
                            
                        }

                    }
                    
                    goalList.add(g);
                    saveGoal();
                    
                }    

            }
            
        } catch (FileNotFoundException ex) {
            
            ex.printStackTrace();
            
        }

    }
    
    /**
     * Read the data from history.txt file
     */      
    public void ScanHistoryFile(){
    
        try {
            
            Scanner scan = new Scanner(files.getHistoryFile());
            String delim = ",";
            
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            while (scan.hasNextLine()) {
        
                String line = scan.nextLine();
   
                if(!line.isEmpty()){
                
                Scanner scan2 = new Scanner(line);
                scan2.useDelimiter(delim);
                    
                    while(scan2.hasNext()){
                    
                        int order = Integer.parseInt(scan2.next());
                        String ID = scan2.next();
                        Date date = null;

                        try {

                            date =formatter.parse(scan2.next());
                            
                        } catch (ParseException ex) {

                            ex.printStackTrace();

                        }

                        int weight = Integer.parseInt(scan2.next());
                        HistoryWeight hw = new HistoryWeight(order, ID, date, weight);
                        
                        if(!checkOrder(order)){
                            
                            historyList.add(hw);
                            
                        }
                            
                    }
                    
                    saveWeight();
                    
                }    

            }
            
        } catch (FileNotFoundException ex) {
            
            ex.printStackTrace();
            
        }

    }    
    
    private boolean checkOrder(int order){
    
        ListIterator it = historyList.listIterator();
        
        while(it.hasNext()){
        
            HistoryWeight hw = (HistoryWeight) it.next();
            if(hw.getOrder() == order){
            
                return true;
            
            }
        
        }
        
        return false;
        
    }

    /**
     * Update the weight of User to a new weight
     * 
     * @param U             User to be updated
     * @param newWeight     new weight
     */      
    public void UpdateWeight(User U, int newWeight){
            
        Iterator it = userList.listIterator();
        
        int count = 0;
        int orderCount = historyList.size();
 
        while (it.hasNext()) {

            User temp = (User) it.next();
            if (temp.getID().equals(U.getID())){

                userList.get(count).setWeight(newWeight);
                HistoryWeight hw = new HistoryWeight(orderCount, U.getID(), new Date(), newWeight);
                historyList.add(hw);
                saveWeight();
                
            } 
            count++;
            
        }
        
        ResetUser(U);
          
    }
    
    /**
     *  Create a new goal with certain info
     * 
     * @param Name      Name of the goal
     * @param Deadline  Deadline of the goal
     * @param target    Target of the goal
     */
    public void CreateGoal(String Name, Date Deadline, int target){
    
        Goal g = new Goal( Name, Deadline, target);

        if(ProfileGUI.u.getWeight()<=target){

            g.setAchieved(true);
            g.setType("Achieved");

        }else{

            g.setAchieved(false);    
            g.setType("Current");

        }    

        goalList.add(g);
        saveGoal();
    
    }    
    
    /**
     * Login the system
     * 
     * @param ID        User ID
     * @param Password  User Password
     */
    public void Login(String ID, String Password){

        ScanUserFile();

        if(userList.size()>0){
            
            Iterator it = userList.listIterator();
            while (it.hasNext()) {

                User temp = (User) it.next();
                
                if (temp.getID().equals(ID)){

                    if(temp.getPassword().trim().equals(Password)){

                        run.lg.dispose();
                        DisplayProfile(temp);
                        ScanGoalFile();
                        ScanHistoryFile();
                        break;
                        
                    }else{

                        run.lg.WrongPassword();
                        break;
                        
                    }    

                }

            }
            if(!IDexist(ID)){
            
                run.lg.noUser();
            
            }
                
        }else
        
            run.lg.noUser();
        
    }  
    
    /**
     * Show the profile of User
     * 
     * @param U User
     */
    public void DisplayProfile(User U){
    
        pg = new ProfileGUI(U);
        pg.setVisible(true);
    
    }    
    
    /**
     * Access the Profile interface
     * 
     * @return Profile interface
     */
    public ProfileGUI getPG(){
    
        return pg;
    
    }

    /**
     * Check if ID is already existed
     *  
     * @param id User ID
     * @return true if ID exist, otherwise, return false
     */
    public boolean IDexist(String id){
    
        if(id.isEmpty()){
        
            return false;
        
        }
        
        Iterator it = userList.listIterator();

        while (it.hasNext()) {
            
            User temp = (User) it.next();
            
            if (temp.getID().equals(id)){
                
                return true;
                
            }
            
        }  
        
        return false;
    
    }
 
   /**
     * Check if Email is already existed
     *  
     * @param email User Email
     * @return true if Email exist, otherwise, return false
     */    
    public boolean emailExist(String email){
        
        Iterator it = userList.listIterator();

        while (it.hasNext()) {
            
            User temp = (User) it.next();
            
            if (temp.getEmail().equals(email)){
                
                return true;
                
            }
            
        }  
        
        return false;
    
    }    

    // Find the age of User
    private int countAge(Date birthday){
    
        Calendar dob = Calendar.getInstance();
        dob.setTime(birthday);
        Calendar today = Calendar.getInstance();
        
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
            age--;
        
        return age;         
        
    }
    
    /**
     * Access the FileStorageController
     * 
     * @return files FileStorageController
     */
    public FileStorageController accessFiles(){
    
        return files;
    
    } 
    
    /**
     * Access the HistoryWeight ArrayList
     * 
     * @return historyList HistoryWeight ArrayList
     */
    public ArrayList<HistoryWeight> accessHW(){
    
        return historyList;
    
    }     
    
    /**
     * Access the Goal ArrayList
     * 
     * @return goalList Goal ArrayList
     */
    public ArrayList<Goal> accessGoal(){
    
        return goalList;
    
    }

    // Check if it is the same date
    private boolean checkDate(Date date){
    
        ListIterator it = historyList.listIterator();
        while(it.hasNext()){
        
            HistoryWeight hw = (HistoryWeight) it.next();
            if(hw.getDate().equals(date)){
            
                return true;
            
            }
        
        }
        
        return false;
    
    }
    
    /**
     * Reset the data of an User after the change
     * 
     * @param u User
     */
    public void ResetUser(User u){
    
        Iterator it = userList.listIterator();
        
        int count = 0;
        
        while (it.hasNext()) {

            User temp = (User) it.next();
            if (temp.getID().equals(u.getID())){

                // remove user with old info
                userList.remove(userList.get(count));
                
                // add the new one to list
                userList.add(u);
                
            } 
          
            count++;
            
        }        
        
        saveToFile();
    
    }
    
    /**
     * Rewrite the history.txt 
     */
    public void saveWeight(){
        
        PrintWriter writer;
        try {
            writer = new PrintWriter(files.getHistoryFile());
            writer.print("");
            writer.close();   
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        
        files.StoreWeight(historyList);
    
    }    
    
    //Rewrite the user.txt 
    private void saveToFile(){
    
        PrintWriter writer;
        try {
            writer = new PrintWriter(files.getUserFile());
            writer.print("");
            writer.close();   
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        
        ListIterator it = userList.listIterator();
        while(it.hasNext()){
        
            files.Store((User) it.next());
        
        }
        
    }
    
    //Rewrite the goal.txt 
    private void saveGoal(){
    
        PrintWriter writer;
        try {
            writer = new PrintWriter(files.getGoalFile());
            writer.print("");
            writer.close();   
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        
        ListIterator it = goalList.listIterator();
        while(it.hasNext()){
        
            files.StoreGoal((Goal) it.next(), ProfileGUI.u); 
        
        }
        
    }    
    
}
