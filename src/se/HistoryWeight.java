package se;

import java.util.Date;

/**
 * 5012B Implementation Reassessment
 * Health Tracker
 * @author Louise Lee
 */
public class HistoryWeight {
    
    private int order;
    private String ID;
    private Date date;
    private int weight;
    
    /**
     * Class that define a weight record
     * 
     * @param order        Record order
     * @param ID        ID of User
     * @param date      Date that the weight being added
     * @param weight    Weight at that time
     */
    public HistoryWeight(int order, String ID, Date date, int weight){
    
        this.order = order;
        this.ID = ID;
        this.date = date;
        this.weight = weight;
    
    }
 
    /**
     * Get order of record
     * 
     * @return order of record
     */
    public int getOrder(){
    
        return order;
    
    }    
    
    /**
     * Get ID of User
     * 
     * @return ID of User
     */
    public String getID(){
    
        return ID;
    
    }
    
    /**
     * Get Date that the weight being added
     * 
     * @return Date that the weight being added
     */
    public Date getDate(){
    
        return date;
    
    }
    
    /**
     * Get Weight at that time
     * 
     * @return Weight at that time
     */
    public int getWeight(){
    
        return weight;
    
    }
    
}
