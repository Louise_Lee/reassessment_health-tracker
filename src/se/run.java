package se;


/**
 * 5012B Implementation Reassessment
 * Health Tracker
 * @author Louise Lee
 */
public class run {
    
    static ModelController mc;
    static LoginGUI lg;
    
    public static void main(String[] args) {
        
        mc = new ModelController();
        lg = new LoginGUI();
        lg.setVisible(true);

    }
    
}
