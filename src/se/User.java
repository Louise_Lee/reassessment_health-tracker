package se;

import java.io.File;
import java.util.Date;

/**
 * 5012B Implementation Reassessment
 * Health Tracker
 * @author Louise Lee
 */
public class User {

    //Details for login
    private String ID;
    private String Password;
    
    //Personal info
    private String Name;
    private String gender;
    private String email;
    private Date dob;
    
    //Need changes
    
    private int Age;
    private int Weight;
    private int height;
    private File photo;    
    
    /**
     * Create a new user with certain details
     * 
     * @param ID        User ID
     * @param Password  User Password
     * @param Name      User Name
     * @param gender    User gender
     * @param email     User email
     * @param dob       User Date of birth
     * @param Age       User Age
     * @param Weight    User weight
     * @param height    User height
     * @param photo     User profile picture
     */
    public User(String ID, String Password, String Name, String gender, 
                String email, Date dob, int Age, int Weight, int height, 
                File photo){
    
        this.ID = ID;
        this.Password = Password;
        this.Name = Name;
        this.gender = gender;
        this.email = email;
        this.dob = dob;
        this.Age = Age;
        this.Weight = Weight;
        this.height = height;
        this.photo = photo;    

    }
    
    /**
     *  Set User ID to another ID
     * 
     * @param ID another ID
     */
    public void setID(String ID){
    
        this.ID = ID;
    
    }
 
    /**
     *  Set User Password to another Password
     * 
     * @param Password another Password
     */    
    public void setPassword(String Password){
    
        this.Password = Password;
    
    }

    /**
     *  Set User Name to another Name
     * 
     * @param Name another Name
     */    
    public void setName(String Name){
    
        this.Name = Name;
    
    }

    /**
     * Set User gender to another gender
     * 
     * @param gender  User gender
     */
    public void setGender(String gender){
    
        this.gender = gender;
    
    }

    /**
     * Set User email to another email
     * 
     * @param email  User email
     */    
    public void setEmail(String email){
    
        this.email = email;
    
    }

    /**
     * Set User Date of birth to another Date of birth
     * 
     * @param dob  User Date of birth
     */        
    public void setDOB(Date dob){
    
        this.dob = dob;
    
    }


    /**
     * Set User Age to another Age
     * 
     * @param Age  User Age
     */       
    public void setAge(int Age){
    
        this.Age = Age;
    
    }

    /**
     * Set User Weight to another Weight
     * 
     * @param Weight  User Weight
     */        
    public void setWeight(int Weight){
    
        this.Weight = Weight;
    
    }

    /**
     * Set User height to another height
     * 
     * @param height  User height
     */      
    public void setHeight(int height){
    
        this.height = height;
    
    }

    /**
     * Set User photo to another photo
     * 
     * @param photo  User photo
     */       
    public void setPhoto(File photo){
    
        this.photo = photo; 
    
    }    
 
    /**
     * Get User ID
     * 
     * @return ID  User ID
     */        
    public String getID(){
    
        return ID;
    
    }

    /**
     * Get User Password
     * 
     * @return Password  User Password
     */         
    public String getPassword(){
    
        return Password;
    
    }

    /**
     * Get User Name
     * 
     * @return Name  User Name
     */     
    public String getName(){
    
        return Name;
    
    }

    /**
     * Get User gender
     * 
     * @return gender  User gender
     */        
    public String getGender(){
    
        return gender;
    
    }

    /**
     * Get User email
     * 
     * @return email  User email
     */      
    public String getEmail(){
    
        return email;
    
    }

    /**
     * Get User date of birth
     * 
     * @return dob  User date of birth
     */          
    public Date getDOB(){
    
        return dob;
    
    }

    /**
     * Get User Age
     * 
     * @return Age  User Age
     */        
    public int getAge(){
    
        return Age;
    
    }

    /**
     * Get User Weight
     * 
     * @return Weight  User Weight
     */        
    public int getWeight(){
    
        return Weight;
    
    }

    /**
     * Get User height
     * 
     * @return height  User height
     */        
    public int getHeight(){
    
        return height;
    
    }

    /**
     * Get User profile picture
     * 
     * @return photo  User profile picture
     */       
    public File getPhoto(){
    
        return photo; 
    
    }      
    
    @Override
    public String toString(){
    
        StringBuilder userInfo = new StringBuilder();
        
            userInfo.append("ID: ");
            userInfo.append(getID());
            userInfo.append("\n");
            userInfo.append("Password: ");
            userInfo.append(getPassword());
            userInfo.append("\n");
            userInfo.append("Name: ");
            userInfo.append(getName());
            userInfo.append("\n");    
            userInfo.append("Gender: ");
            userInfo.append(getGender());
            userInfo.append("\n");                
            userInfo.append("Email: ");
            userInfo.append(getEmail());
            userInfo.append("\n");   
            userInfo.append("Date of Birth: ");
            userInfo.append(getDOB());
            userInfo.append("\n");   
            userInfo.append("Age: ");
            userInfo.append(getAge());
            userInfo.append("\n");   
            userInfo.append("Weight: ");
            userInfo.append(getWeight());
            userInfo.append("\n");       
            userInfo.append("Height: ");
            userInfo.append(getHeight());
            userInfo.append("\n");   
            userInfo.append("Photo Path: ");
            userInfo.append(getPhoto());
            userInfo.append("\n");       
        
        return userInfo.toString();

    }
    
}
