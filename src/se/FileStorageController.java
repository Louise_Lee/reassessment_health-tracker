package se;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * 5012B Implementation Reassessment
 * Health Tracker
 * @author Louise Lee
 */
public class FileStorageController{
    
    private File user_File;
    private File goal_File;
    private File history_File;
    
    public FileStorageController(){
    
        user_File = new File("storage\\user.txt");
        goal_File = new File("storage\\goal.txt");
        history_File = new File("storage\\history.txt");
    
    }
    
    /**
     * Access the user.txt file
     * 
     * @return File user_File
     */
    public File getUserFile(){
    
        return user_File;
    
    }
    
    /**
     * Access the goal.txt file
     * 
     * @return File goal_File
     */
    public File getGoalFile(){
    
        return goal_File;
    
    }    
    
    /**
     * Access the history.txt file
     * 
     * @return File history_File
     */
    public File getHistoryFile(){
    
        return history_File;
    
    }        
    
    /**
     * Store the user to the user.txt file
     * 
     * @param U user to be stored
     */
    public void Store(User U){
    
        try(FileWriter fw = new FileWriter("storage\\user.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)){
            
            out.print(U.getID());
            out.print(',');
            out.print(U.getPassword());
            out.print(',');
            out.print(U.getName());
            out.print(',');
            out.print(U.getGender());
            out.print(',');
            out.print(U.getEmail());
            out.print(',');
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            out.print(df.format(U.getDOB()));
            out.print(',');
            out.print(U.getAge());
            out.print(',');
            out.print(U.getWeight());
            out.print(',');
            out.print(U.getHeight());
            out.print(',');
            out.print(U.getPhoto());            
            out.println();

        }catch (IOException e) {

            e.printStackTrace();
            
        }    
    
    }

    
    /**
     * Store the goal to the goal.txt file
     * 
     * @param G goal
     * @param U user 
     */
    public void StoreGoal(Goal G,User U){
    
        try(FileWriter fw = new FileWriter("storage\\goal.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)){
            
            out.print(G.getName());
            out.print(',');
            out.print(G.getID());
            out.print(',');
            out.print(G.getType());
            out.print(',');
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            out.print(df.format(G.getDeadline()));
            out.print(',');
            out.print(G.getAchieved());
            out.print(',');
            out.print(G.getTarget());            
            out.println();

        }catch (IOException e) {

            e.printStackTrace();
            
        }        
    
    }  
    
    /**
     * Store the weight to history.txt
     * 
     * @param hw ArrayList that contains Weight record of all User 
     */
    public void StoreWeight(ArrayList<HistoryWeight> hw){
    
        try(FileWriter fw = new FileWriter("storage\\history.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)){
            
            ListIterator it = hw.listIterator();
            while(it.hasNext()){
            
                HistoryWeight temp = (HistoryWeight) it.next();
                out.print(temp.getOrder());
                out.print(',');
                out.print(temp.getID());
                out.print(',');
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                out.print(df.format(temp.getDate()));
                out.print(',');
                out.print(temp.getWeight());
                out.println();
                
            }

        }catch (IOException e) {

            e.printStackTrace();
            
        }        
    
    }     

}
